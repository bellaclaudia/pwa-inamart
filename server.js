const { createServer } = require('http')
const { join } = require('path')
const { parse } = require('url')
const next = require('next')

const port = parseInt(process.env.port, 10) || 3001
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app.prepare()
.then(() => {
    createServer((req, res) => {
        const parsedUrl = parse(req.url, true)
        const { pathname } = parsedUrl
        if (pathname === '/service-worker.js') {
            const filePath = join(__dirname, '.next', pathname)
            app.serverStatic(req, res, filePath)
        } else {
            handle(req, res, parsedUrl)
        }
    })
    .listen(port, (err) => {
        if (err) throw err
    })
})
