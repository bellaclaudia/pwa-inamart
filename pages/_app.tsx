import { ChakraProvider } from "@chakra-ui/react"

export default function App({Component, pageProops}) {
    return (
        <ChakraProvider>
            <Component {... pageProops} />
        </ChakraProvider>
    )
}