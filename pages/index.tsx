import {Button, Flex, Heading, Input, useColorMode, useColorModeValue} from '@chakra-ui/react'
import Head from "next/head"

const IndexPage = () => {
  const { toggleColorMode } = useColorMode()
  const formBackground = useColorModeValue("grey.100", "grey.700")
  return <div>
    <Head>
      <title>{ "title"} </title>
      <link rel="icon" href="/static/icon.png" />
      <meta name="viewport" content="width=device-width, initial-scale=1" />
      <meta name="theme-color" content="#000000" />
      <meta
        name="description"
        content="PWA Inamart"
      />
      <link rel="apple-touch-icon" href="/icon.png" />
      <link rel="manifest" href="/static/manifest.json" />
    </Head>
    <div>
        <Flex height="100vh" alignItems="center" justifyContent="center">
          <Flex direction="column" background={formBackground} p={12}>
            <Heading mb={6}> Log in</Heading>
            <Input placeholder="Isi Email Anda" variant="filled" mb={3} type="email" />
            <Input placeholder="*******" variant="filled" mb={6} type="password" />
            <Button mb={6} colorScheme="teal">Log In</Button>
            {/* <Button onClick={toggleColorMode}>Toggle Color Mode</Button> */}
          </Flex>
        </Flex>
      </div>
    </div>
}

export default IndexPage
